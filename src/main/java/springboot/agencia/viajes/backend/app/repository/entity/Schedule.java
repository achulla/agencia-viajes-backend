package springboot.agencia.viajes.backend.app.repository.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "schedule")
public class Schedule {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @Column(name = "hora")
    private String hora;



    /***********************GETTERS AND SETTERS*********************/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
