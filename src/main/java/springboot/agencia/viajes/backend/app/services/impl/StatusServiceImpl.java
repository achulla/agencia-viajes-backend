package springboot.agencia.viajes.backend.app.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.StatusDto;
import springboot.agencia.viajes.backend.app.repository.interfaces.StatusRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.StatusService;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }


    @Override
    public List<StatusDto> obtenerDtoList() {
        return statusRepository.obtenerDtoList();
    }


}
