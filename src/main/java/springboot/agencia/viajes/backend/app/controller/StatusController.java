package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.agencia.viajes.backend.app.dto.StatusDto;
import springboot.agencia.viajes.backend.app.services.interfaces.StatusService;

import java.util.List;

@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/status")
@Api(tags = "Rest Api Status", consumes = "application/json")
public class StatusController {

    private final StatusService statusService;

    @Autowired
    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }


    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar los usuarios")
    public List<StatusDto> obtenerDtoList(){
        return statusService.obtenerDtoList();
    }



}
