package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("price")
public class PriceDto {
    @ApiModelProperty("El id del Precio")
    private Long id;

    @ApiModelProperty("El id de la Origen")
    private long id_origen;

    @ApiModelProperty("Descripcion del Origen")
    private Long id_ciudad_origen;

    @ApiModelProperty("Descripcion del Origen")
    private String descripcion_origen;

    @ApiModelProperty("El id de la Destino")
    private long id_destino;

    @ApiModelProperty("Descripcion del Origen")
    private Long id_ciudad_destino;

    @ApiModelProperty("Descripcion del Origen")
    private String descripcion_destino;

    @ApiModelProperty("El id de la Transporte")
    private long id_transporte;

    @ApiModelProperty("Tipo del Trasporte")
    private String tipo_transporte;

    @ApiModelProperty("Importe del Precio")
    private double importe;

    public PriceDto(){}

    public PriceDto( Long id, Long id_origen, Long id_ciudad_origen, String descripcion_origen, Long id_destino, Long id_ciudad_destino, String descripcion_destino, Long id_transporte, String tipo_transporte , Double importe){

        this.id=id;
        this.id_origen=id_origen;
        this.id_ciudad_origen=id_ciudad_origen;
        this.descripcion_origen=descripcion_origen;
        this.id_destino=id_destino;
        this.id_ciudad_destino=id_ciudad_destino;
        this.descripcion_destino=descripcion_destino;
        this.id_transporte=id_transporte;
        this.tipo_transporte=tipo_transporte;
        this.importe=importe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getId_origen() {
        return id_origen;
    }

    public void setId_origen(long id_origen) {
        this.id_origen = id_origen;
    }

    public Long getId_ciudad_origen() {
        return id_ciudad_origen;
    }

    public void setId_ciudad_origen(Long id_ciudad_origen) {
        this.id_ciudad_origen = id_ciudad_origen;
    }

    public String getDescripcion_origen() {
        return descripcion_origen;
    }

    public void setDescripcion_origen(String descripcion_origen) {
        this.descripcion_origen = descripcion_origen;
    }

    public long getId_destino() {
        return id_destino;
    }

    public void setId_destino(long id_destino) {
        this.id_destino = id_destino;
    }

    public Long getId_ciudad_destino() {
        return id_ciudad_destino;
    }

    public void setId_ciudad_destino(Long id_ciudad_destino) {
        this.id_ciudad_destino = id_ciudad_destino;
    }

    public String getDescripcion_destino() {
        return descripcion_destino;
    }

    public void setDescripcion_destino(String descripcion_destino) {
        this.descripcion_destino = descripcion_destino;
    }

    public long getId_transporte() {
        return id_transporte;
    }

    public void setId_transporte(long id_transporte) {
        this.id_transporte = id_transporte;
    }

    public String getTipo_transporte() {
        return tipo_transporte;
    }

    public void setTipo_transporte(String tipo_transporte) {
        this.tipo_transporte = tipo_transporte;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }


}
