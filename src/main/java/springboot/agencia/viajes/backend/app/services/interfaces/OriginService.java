package springboot.agencia.viajes.backend.app.services.interfaces;

import springboot.agencia.viajes.backend.app.dto.OriginDto;

import java.util.List;

public interface OriginService {


/**
 * Obtiene Todos los datos del Origen
 *
 */
    List<OriginDto> obtenerDtoList();

/**
 * Obtiene los datos de la Origen
 *
 * @param idOrigin Id del Origen
 *      * @return origen
 */
    OriginDto findByIdOrigin(Long idOrigin);





}
