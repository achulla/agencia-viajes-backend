package springboot.agencia.viajes.backend.app.services.interfaces;
import springboot.agencia.viajes.backend.app.dto.PriceDto;

import java.util.List;


public interface PriceService {

    /**
     * Obtiene Todos los datos de los precios
     *
     */
    List<PriceDto> obtenerDtoList();

}
