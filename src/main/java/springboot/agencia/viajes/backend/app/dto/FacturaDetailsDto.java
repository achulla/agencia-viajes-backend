package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("Factura (Detalle)")
public class FacturaDetailsDto {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("El id del Usuario")
    private Long id_user;

    @ApiModelProperty("Fecha de Registro")
    private Date fechaRegistro;

    public FacturaDetailsDto(Long id, Long id_user, Date fechaRegistro) {
        this.id = id;
        this.id_user = id_user;
        this.fechaRegistro = fechaRegistro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
