package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.DestinationDto;
import springboot.agencia.viajes.backend.app.repository.entity.Destination;

import java.util.List;

@Repository
public interface DestinationRepository extends JpaRepository<Destination, Long> {

        @Query("select new springboot.agencia.viajes.backend.app.dto.DestinationDto (" +
                "d.id," +
                "d.city.ciudad ) from Destination d  ")
        List<DestinationDto> obtenerDtoList();

/*        @Query("select new springboot.agencia.viajes.backend.app.dto.DestinationDto (" +
                "d.id," +
                "d.city,) from Destination d  where d.id =:id order by d.id desc")
        Page<DestinationDto> findDtoById(@Param("id") Long id);*/


}
