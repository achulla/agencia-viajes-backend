package springboot.agencia.viajes.backend.app.services.interfaces;

import springboot.agencia.viajes.backend.app.dto.PaymentDto;

import java.util.List;

public interface PaymentService {

    List<PaymentDto> obtenerDtoList();

     PaymentDto findById(Long id);

    Long createReservation(PaymentDto dto);

    PaymentDto updatePayment(Long id, PaymentDto paymentDto);


     void deleteById(Long id);

}
