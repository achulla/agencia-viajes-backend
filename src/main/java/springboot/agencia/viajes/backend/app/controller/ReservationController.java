package springboot.agencia.viajes.backend.app.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.PaymentReservationDto;
import springboot.agencia.viajes.backend.app.dto.ReservationDto;
import springboot.agencia.viajes.backend.app.services.interfaces.ReservationService;

import java.util.List;

@RestController
@RequestMapping("/api/reserv")
@CrossOrigin(origins= "http://localhost:4200")
@Api(tags = "Rest Api Reservation", consumes = "application/json")
public class ReservationController {

    private final ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar las Reservaciones")
    public List<ReservationDto> obtenerDtoList(){
        return reservationService.obtenerDtoList();
    }

    @PostMapping("/save")
    @ApiOperation(value = "guardar", notes = "guardar las reservaciones")
    public ReservationDto saveReservation (@RequestBody ReservationDto reservationDto){

        return reservationService.saveReservation(reservationDto);
    }

    @PostMapping("/payment")
    @ApiOperation(value = "pagar", notes = "pagar la reservacion")
    public ReservationDto payment (@RequestBody PaymentReservationDto dto){

        return reservationService.payment(dto);
    }


}
