package springboot.agencia.viajes.backend.app.repository.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "factura")
public class Factura extends BasicEntity{

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
