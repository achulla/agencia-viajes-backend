package springboot.agencia.viajes.backend.app.dto;


import java.io.Serializable;

public class ScheduleReservationDto implements Serializable {

        private String hour;
        private String origen;
        private String destination;
        private String transportType;
        private String price;
        private String identifierPrice;

    public String getIdentifierPrice() {
        return identifierPrice;
    }

    public void setIdentifierPrice(String identifierPrice) {
        this.identifierPrice = identifierPrice;
    }

    public String getHour() {
            return hour;
        }

        public void setHour(String hour) {
            this.hour = hour;
        }

        public String getOrigen() {
            return origen;
        }

        public void setOrigen(String origen) {
            this.origen = origen;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getTransportType() {
            return transportType;
        }

        public void setTransportType(String transportType) {
            this.transportType = transportType;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }


