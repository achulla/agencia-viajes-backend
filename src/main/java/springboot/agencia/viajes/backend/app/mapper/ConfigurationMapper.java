package springboot.agencia.viajes.backend.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import springboot.agencia.viajes.backend.app.dto.FacturaDetailsDto;
import springboot.agencia.viajes.backend.app.dto.FacturaDto;
import springboot.agencia.viajes.backend.app.repository.entity.Factura;

@Mapper(componentModel = "spring")
public interface ConfigurationMapper {

    @Mappings({
            @Mapping(target = "id", source = "entity.id"),
            @Mapping(target = "id_user", source = "entity.user.id"),
            @Mapping(target = "fechaRegistro", source = "entity.fechaRegistro"),
    })
    FacturaDetailsDto facturaDetailsDto(Factura entity);

}
