package springboot.agencia.viajes.backend.app.dto;

import java.io.Serializable;
import java.util.List;

public class ResponseScheduleDto implements Serializable {

    private String travelerDate;
    private List<ScheduleReservationDto> response;




    public String getTravelerDate() {
        return travelerDate;
    }

    public void setTravelerDate(String travelerDate) {
        this.travelerDate = travelerDate;
    }

    public List<ScheduleReservationDto> getResponse() {
        return response;
    }

    public void setResponse(List<ScheduleReservationDto> response) {
        this.response = response;
    }
}
