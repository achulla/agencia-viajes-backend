package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.agencia.viajes.backend.app.dto.DestinationDto;
import springboot.agencia.viajes.backend.app.repository.entity.Destination;
import springboot.agencia.viajes.backend.app.repository.interfaces.DestinationRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.DestinationService;

import java.util.List;
import java.util.Optional;

@Service
public class DestinationServiceImpl implements DestinationService {



    private final DestinationRepository destinationRepository;

    @Autowired
    public DestinationServiceImpl(DestinationRepository destinationRepository) {
        this.destinationRepository = destinationRepository;
    }

    @Override
    public List<DestinationDto> obtenerDtoList() {
        return destinationRepository.obtenerDtoList();
    }

    @Override
    @Transactional("transactionManager")
    public DestinationDto findByIdDest(Long idDest) {
        Optional<Destination> destOptional = destinationRepository.findById(idDest);
        return destinationToDto(destOptional.get());
    }

/*    @Override
    @Transactional("transactionManager")
    public Long createDest(DestinationDto dto) {

        Destination destination = new Destination();

        destination.setId(dto.getId());
        destination.setOrigen(dto.getOrigen());
        destination.setDestino(dto.getDestino());
        destination.setPrecio(dto.getPrecio());

        destination = destinationRepository.save(destination);

        return destination.getId();

    }*/

/*

    @Override
    @Transactional("transactionManager")
    public DestinationDto updateDest(Long idDest, DestinationDto destinationDto) {
        Optional<Destination> destOptional = destinationRepository.findById(idDest);

        Destination updateDest = destOptional.get();

        updateDest.setId(destinationDto.getId());
        updateDest.setOrigen(destinationDto.getOrigen());
        updateDest.setDestino(destinationDto.getDestino());
        updateDest.setPrecio(destinationDto.getPrecio());

        updateDest = destinationRepository.save(updateDest);

        return destinationToDto(updateDest);
    }
*/



/*    @Override
    public void deleteById(Long idDest) {

        destinationRepository.deleteById(idDest);
    }*/


    /***********************METODOS PARA COMPLEMENTAR*****************************/

    private DestinationDto destinationToDto(Destination destination){

        DestinationDto destinationDto = new DestinationDto();
        destinationDto.setId(destination.getId());
        return destinationDto;
    }





}
