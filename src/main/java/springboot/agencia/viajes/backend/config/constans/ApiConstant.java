package springboot.agencia.viajes.backend.config.constans;

public class ApiConstant {

    private ApiConstant(){

    }
    public static final String CONTENT_TYPE="application/json";

    // public static final String HEADER_AUTORIZACION ="Reservation";
    public static final String BEARER="Bearer";
    public static final String JSON_DATA="$.content";

    public static final String BASE_PATH = "api/schedule/reservation";


}
