package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.TransportDto;
import springboot.agencia.viajes.backend.app.repository.entity.Transport;

import java.util.List;

@Repository
public interface TransportRepository extends JpaRepository<Transport, Long> {

        @Query("select new springboot.agencia.viajes.backend.app.dto.TransportDto (" +
           "t.id," +
           "t.placa," +
           "t.marca," +
           "t.chofer," +
           "t.tipo_vehiculo," +
           "t.estado ) from Transport t  ")
        List<TransportDto> obtenerDtoList();


}
