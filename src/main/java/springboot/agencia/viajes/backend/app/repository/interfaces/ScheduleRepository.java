package springboot.agencia.viajes.backend.app.repository.interfaces;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.ScheduleDto;
import springboot.agencia.viajes.backend.app.repository.entity.Schedule;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {




    @Query("select new springboot.agencia.viajes.backend.app.dto.ScheduleDto(  " +
            "s.id," +
            "s.hora" +
            ") from Schedule s ")
            List<ScheduleDto> obtenerDtoList();



}
