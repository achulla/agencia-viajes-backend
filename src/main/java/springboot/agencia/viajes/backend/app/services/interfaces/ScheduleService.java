package springboot.agencia.viajes.backend.app.services.interfaces;

import springboot.agencia.viajes.backend.app.dto.ScheduleDto;

import java.util.List;

public interface ScheduleService {

/**
 * Obtiene Todos los datos de Horario
 *
 */
    List<ScheduleDto> obtenerDtoList();


/**
 * Obtiene los datos del Horario
 *
 * @param id Id del Horario
 *      * @return Horario
 */
    ScheduleDto findById(Long id);



/**
 * Registrar Horario
 *
 * @param dto Horario
 * @return registro de un Horario
 */
    Long createSchedule(ScheduleDto dto);



/**
 * Modifica un transporte segun los datos enviados.
 *
 * @param id  Id del Horario
 * @param scheduleDto Horario
 * @return Los campos del traHorarionsporte seran modificados
 */
    ScheduleDto updateSchedule(Long id, ScheduleDto scheduleDto);


/**
 * Eliminar un Horario del repositorio de datos
 *
 * @param id Id del Horario
 */
    void deleteById(Long id);




}
