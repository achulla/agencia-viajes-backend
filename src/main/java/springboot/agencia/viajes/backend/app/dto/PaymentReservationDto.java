package springboot.agencia.viajes.backend.app.dto;

import java.io.Serializable;

public class PaymentReservationDto implements Serializable {

    private String idReservation;
    private String status;

    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
