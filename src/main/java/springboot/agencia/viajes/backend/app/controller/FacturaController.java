package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.FacturaDetailsDto;
import springboot.agencia.viajes.backend.app.dto.FacturaDto;
import springboot.agencia.viajes.backend.app.dto.FacturaRequestDto;
import springboot.agencia.viajes.backend.app.services.interfaces.FacturaService;
import springboot.agencia.viajes.backend.config.constans.HeaderConstants;
import springboot.agencia.viajes.backend.config.sort.SortEntityEnum;
import springboot.agencia.viajes.backend.config.sort.SortMapUtil;
import springboot.agencia.viajes.backend.util.MessageUtil;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/factura")
@Api(tags = "Rest Api City", consumes = "application/json")
public class FacturaController {

    private final FacturaService facturaService;
    private final MessageUtil messageUtil;
    private final SortMapUtil sortMapUtil;

    public FacturaController(FacturaService facturaService,
                             MessageUtil messageUtil,
                             SortMapUtil sortMapUtil) {
        this.facturaService = facturaService;
        this.messageUtil = messageUtil;
        this.sortMapUtil = sortMapUtil;
    }

    @GetMapping
    @ApiOperation(value = "Listado de Facturas", notes = "Permite obtener todos las Facturas")
    public Page<FacturaDto> getFacturas(@RequestParam(value = "filter", required = false) String filter, Pageable pageable){
        pageable = sortMapUtil.updatePagination(pageable, SortEntityEnum.FACTURA);
        return facturaService.getFacturas(filter, pageable);
    }

    @GetMapping("/{idFactura}")
    @ApiOperation(value = "Listado de Facturas", notes = "Permite obtener todos las Facturas")
    public FacturaDetailsDto getFacturas(@PathVariable("idFactura") Long idFactura){
        return facturaService.buscarFactura(idFactura);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Crear Factura", notes = "Registrar una nueva Factura")
    public FacturaDetailsDto saveFactura(@RequestBody FacturaRequestDto facturaRequestDto, HttpServletResponse response){
        FacturaDetailsDto factura = facturaService.saveFactura(facturaRequestDto);
        response.setHeader(HeaderConstants.MESSAGES, messageUtil.getMessageByCode("Factura Creado"));
     return factura;
    }

    @DeleteMapping("/{idFactura}")
    @ApiOperation(value = "Eliminar Factura", notes = "Permite eliminar una Factura")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFacturaBy(@PathVariable("idFactura") Long idFactura, HttpServletResponse response){
        facturaService.deleteFactura(idFactura);
        response.setHeader(HeaderConstants.MESSAGES, messageUtil.getMessageByCode("Factura Eliminado"));
    }

    @PutMapping("/{idFactura}")
    @ApiOperation(value = "Modificar Factura", notes = "Permite modificar una Factura")
    public FacturaDetailsDto updateFactura(@RequestBody @Valid FacturaRequestDto facturaRequestDto,
                                           @PathVariable("idFactura") Long idFactura,
                                           HttpServletResponse response){
        FacturaDetailsDto data = facturaService.updateFactura(facturaRequestDto, idFactura);
        response.setHeader(HeaderConstants.MESSAGES, messageUtil.getMessageByCode("Factura Modificada"));
        return data;
    }

}
