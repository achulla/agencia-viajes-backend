package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.DestinationDto;
import springboot.agencia.viajes.backend.app.services.interfaces.DestinationService;
import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/destination")
@Api(tags = "Rest Api Destination", consumes = "application/json")
public class DestinationController {


    private final DestinationService destinationService;

    @Autowired
    public DestinationController(DestinationService destinationService) {
        this.destinationService = destinationService;
    }

    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar el Destino")
    public List<DestinationDto> obtenerDtoList(){
        return destinationService.obtenerDtoList();
    }

    /*
    @GetMapping("/{idDest}")
    @ApiOperation(value = "Detalle", notes = "Detalle de un destino por Id")
    public DestinationDto detailDestination(@PathVariable("idDestination") Long idDest){
        return destinationService.findByIdDest(idDest);
    }
     */



}
