package springboot.agencia.viajes.backend.app.repository.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BasicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    @Column(name="fechaRegistro")
    private Date fechaRegistro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fechaModificacion")
    private Date fechaModificacion;

    @Column(name="idCreadorPor")
    private Integer idCreadorPor;

    @Column(name="idModificadoPor")
    private Integer idModificadoPor;

    @Column(name="modificadoPor")
    private String modificadoPor;

    @Column(name="creadoPor")
    private String creadoPor;

    @Column(name="flagActivo")
    private Boolean flagActivo;

    @Column(name="flagEstado")
    private Boolean flagEstado;

    @PrePersist
    private void persist(){
        flagEstado = true;
        flagActivo = true;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getIdCreadorPor() {
        return idCreadorPor;
    }

    public void setIdCreadorPor(Integer idCreadorPor) {
        this.idCreadorPor = idCreadorPor;
    }

    public Integer getIdModificadoPor() {
        return idModificadoPor;
    }

    public void setIdModificadoPor(Integer idModificadoPor) {
        this.idModificadoPor = idModificadoPor;
    }

    public String getModificadoPor() {
        return modificadoPor;
    }

    public void setModificadoPor(String modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public String getCreadoPor() {
        return creadoPor;
    }

    public void setCreadoPor(String creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Boolean getFlagActivo() {
        return flagActivo;
    }

    public void setFlagActivo(Boolean flagActivo) {
        this.flagActivo = flagActivo;
    }

    public Boolean getFlagEstado() {
        return flagEstado;
    }

    public void setFlagEstado(Boolean flagEstado) {
        this.flagEstado = flagEstado;
    }
}
