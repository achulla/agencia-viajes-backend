package springboot.agencia.viajes.backend.app.dto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import springboot.agencia.viajes.backend.app.repository.entity.Origin;

@ApiModel("origin")
public class OriginDto {


    @ApiModelProperty("El id del Origen")
    private Long id;

    @ApiModelProperty("El id de la Ciudad")
    private long id_city;

    @ApiModelProperty("Descripcion de la Ciudad")
    private String ciudad;

    public OriginDto(){}

    public OriginDto( Long id, String ciudad ){

        this.id=id;
        this.ciudad=ciudad;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getId_city() {
        return id_city;
    }

    public void setId_city(long id_city) {
        this.id_city = id_city;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
