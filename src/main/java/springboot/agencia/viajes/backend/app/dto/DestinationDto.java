package springboot.agencia.viajes.backend.app.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import springboot.agencia.viajes.backend.app.repository.entity.Destination;

@ApiModel("destination")
public class DestinationDto {

    @ApiModelProperty("El id del Destino")
    private Long id;

    @ApiModelProperty("El id de la Ciudad")
    private Long id_city;

    @ApiModelProperty("Descripcion de la Ciudad")
    private String ciudad;

    public DestinationDto(){}

    public DestinationDto( Long id,  String ciudad ){

        this.id=id;
        this.ciudad=ciudad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_city() {
        return id_city;
    }

    public void setId_city(Long id_city) {
        this.id_city = id_city;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
