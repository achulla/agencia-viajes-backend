package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



@ApiModel("tracking_roadmaps")
public class TrackingRoadMapDto {

    @ApiModelProperty("Id")
    private Long id;

    @ApiModelProperty("Latitude")
    private Double latitude;

    @ApiModelProperty("Longitude")
    private Double longitude;

/*TrackingRoadMapDto(){}*/

/*    TrackingRoadMapDto(Long id, Double latitude, Double longitude){
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }*/


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
