package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.ResponseScheduleDto;
import springboot.agencia.viajes.backend.app.services.interfaces.ScheduleReservationService;

@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping ("api/schedule/reservation")
@Api(tags = "Rest Api Schedule Reservation", consumes = "application/json")
public class ScheduleReservationController {

    private final ScheduleReservationService scheduleReservationService;

    @Autowired
    public ScheduleReservationController(ScheduleReservationService scheduleReservationService){

        this.scheduleReservationService = scheduleReservationService;
    }

    @GetMapping
    public ResponseScheduleDto getSchedules (@RequestParam ("origin") String origin,
                                             @RequestParam ("destination") String destination)
                                                {

        return scheduleReservationService.getSchedules(origin,destination);
    }
}
