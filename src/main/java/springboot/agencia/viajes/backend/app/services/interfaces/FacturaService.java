package springboot.agencia.viajes.backend.app.services.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import springboot.agencia.viajes.backend.app.dto.FacturaDetailsDto;
import springboot.agencia.viajes.backend.app.dto.FacturaDto;
import springboot.agencia.viajes.backend.app.dto.FacturaRequestDto;

public interface FacturaService {

    /**
     * Metodo para Crear
     * @param facturaRequestDto
     * @return
     */
    FacturaDetailsDto saveFactura(FacturaRequestDto facturaRequestDto);

    /**
     * Metodo para Actualizar
     * @param facturaRequestDto
     * @return
     */
    FacturaDetailsDto updateFactura(FacturaRequestDto facturaRequestDto, Long idFactura);

    /**
     * Metodo para traer toda la lista de la factura
     * @param pageable
     * @param filter
     * @return
     */
    Page<FacturaDto> getFacturas(String filter, Pageable pageable);

    /**
     * Metodo para obtener una factura por Id
     * @param idFactura
     * @return
     */
    FacturaDetailsDto buscarFactura(Long idFactura);

    /**
     * Metodo para Eliminar una factura por Id
     * @param idFactura
     */
    void deleteFactura(Long idFactura);

}
