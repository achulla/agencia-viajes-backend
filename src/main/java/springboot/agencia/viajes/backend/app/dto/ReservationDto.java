package springboot.agencia.viajes.backend.app.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import java.util.Date;

@ApiModel("reservation")
public class ReservationDto {

    @ApiModelProperty("El id del reservation")
    private Long id;

    @ApiModelProperty("Esatdo de la reserva")
    private String estado;

    @ApiModelProperty("fecha de registro")
    private Date fecha_registro;

    @ApiModelProperty("fecha de espera")
    private Date fecha_caducidad;

    @ApiModelProperty("identificador de precio")
    private String identifierPrice;

    @ApiModelProperty("identificador de precio")
    private String hora;

    /*
        @ApiModelProperty("El id del usuario")
        private Long user_id;  */
    @ApiModelProperty("nombre del usuario")
    private String name;

    @ApiModelProperty("numero de dni")
    private String dni;

    @ApiModelProperty("apellido del usuario")
    private String lastname;


    /*
    @ApiModelProperty("id del transport")
    private Long transport_id;  */

    @ApiModelProperty("nombre del chofer")
    private String tipo_transporte;

    /*
    @ApiModelProperty("id del horario")
    private Long schedule_id;  */

    @ApiModelProperty("fecha del viaje")
    private String fecha_viaje;

    @ApiModelProperty("Importe del Precio")
    private double importe;

    @ApiModelProperty("id reservacion")
    private String idReservation;

    @ApiModelProperty("origen")
    private String origen;

    @ApiModelProperty("destino")
    private String destino;

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getIdentifierPrice() {
        return identifierPrice;
    }

    public void setIdentifierPrice(String identifierPrice) {
        this.identifierPrice = identifierPrice;
    }

    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }

    public String getDni() {
        return dni;
    }


    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public ReservationDto(Long id, String estado, Date fecha_registro, Date fecha_caducidad, String name, String lastname, String tipo_transporte, String fecha_viaje, double importe, String idReservation) {
        this.id = id;
        this.estado = estado;
        this.fecha_registro = fecha_registro;
        this.fecha_caducidad = fecha_caducidad;
        this.name = name;
        this.lastname = lastname;
        this.tipo_transporte = tipo_transporte;
        this.fecha_viaje = fecha_viaje;
        this.importe = importe;
        this.idReservation = idReservation;
    }

    public ReservationDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Date getFecha_caducidad() {
        return fecha_caducidad;
    }

    public void setFecha_caducidad(Date fecha_caducidad) {
        this.fecha_caducidad = fecha_caducidad;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTipo_transporte() {
        return tipo_transporte;
    }

    public void setTipo_transporte(String tipo_transporte) {
        this.tipo_transporte = tipo_transporte;
    }

    public String getFecha_viaje() {
        return fecha_viaje;
    }

    public void setFecha_viaje(String fecha_viaje) {
        this.fecha_viaje = fecha_viaje;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }
}
