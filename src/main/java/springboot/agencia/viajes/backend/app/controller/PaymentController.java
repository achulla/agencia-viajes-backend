package springboot.agencia.viajes.backend.app.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.agencia.viajes.backend.app.dto.PaymentDto;
import springboot.agencia.viajes.backend.app.services.interfaces.PaymentService;

import java.util.List;

@RestController
@RequestMapping("/api/payment")
@CrossOrigin(origins= "http://localhost:4200")
@Api(tags = "Rest Api Payment", consumes = "application/json")
public class PaymentController {

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }



    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar los usuarios")
    public List<PaymentDto> obtenerDtoList(){
        return paymentService.obtenerDtoList();
    }





}
