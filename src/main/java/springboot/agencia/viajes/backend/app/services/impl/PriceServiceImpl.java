package springboot.agencia.viajes.backend.app.services.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springboot.agencia.viajes.backend.app.dto.PriceDto;

import springboot.agencia.viajes.backend.app.repository.entity.Price;
import springboot.agencia.viajes.backend.app.repository.interfaces.PriceRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.PriceService;

import java.util.ArrayList;
import java.util.List;




@Service
public class PriceServiceImpl implements PriceService{

    private final PriceRepository priceRepository;

    @Autowired
    public PriceServiceImpl(PriceRepository priceRepository){
        this.priceRepository = priceRepository;
    }

    @Override
    public List<PriceDto> obtenerDtoList() {

        List<Price> priceList  = priceRepository.findAll();
        List<PriceDto> priceDtos = new ArrayList<>();

        priceList.stream().forEach(priceEntity -> {
            priceDtos.add(buildPriceDto(priceEntity));
        });


        return priceDtos;
    }

    // metodo para construir el Dto atrraver de una entidad
    private PriceDto buildPriceDto (Price entity){

        PriceDto dto = new PriceDto();

        dto.setDescripcion_destino(entity.getId_destino().getCity().getCiudad());
        dto.setDescripcion_origen(entity.getId_origen().getCity().getCiudad());
        dto.setTipo_transporte(entity.getId_transport().getTipo_vehiculo());
        dto.setImporte(Double.parseDouble(entity.getImporte()));

        //Mostrar por id
       /*dto.setId_origen(entity.getId_origen().getCity().getId());
       dto.setId_destino(entity.getId_destino().getCity().getId());
       dto.setId_transporte(entity.getId_transport().getId());*/


        return dto;

    }

}
