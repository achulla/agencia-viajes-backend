package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("city")
public class CityDto {

    @ApiModelProperty("El id de ciudad")
    private Long id;

    @ApiModelProperty("nombre de ciudad")
    private String ciudad;

    @ApiModelProperty("pais de ciudad")
    private String pais;


    public CityDto(){

    }

    public CityDto(Long id, String ciudad, String pais) {
        this.id = id;
        this.ciudad = ciudad;
        this.pais = pais;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
