package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.Date;

@ApiModel("Factura (Crear/Editar)")
public class FacturaRequestDto {

    @ApiModelProperty(value = "El id del Usuario")
    private Long id_user;

    @ApiModelProperty(value = "Fecha de Registro", required = true)
    @NotNull(message = "El campo fecha es requerido")
    private Date fechaRegistro;

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
