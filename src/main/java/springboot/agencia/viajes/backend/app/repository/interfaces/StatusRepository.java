package springboot.agencia.viajes.backend.app.repository.interfaces;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.StatusDto;
import springboot.agencia.viajes.backend.app.repository.entity.Status;

import java.util.List;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

        @Query("select new springboot.agencia.viajes.backend.app.dto.StatusDto (" +
            "st.id," +
            "st.estado) from Status st  ")
        List<StatusDto> obtenerDtoList();

}
