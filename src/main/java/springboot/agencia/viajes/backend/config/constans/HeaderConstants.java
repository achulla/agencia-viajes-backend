package springboot.agencia.viajes.backend.config.constans;

public class HeaderConstants {
    public static final String MESSAGES = "Custom-Message";
    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String REQUEST_WITH = "X-Requested-With";
    public static final String ORIGIN = "Origin";

    private HeaderConstants() {
    }
}
