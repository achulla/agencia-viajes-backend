package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("status")
public class StatusDto {


    @ApiModelProperty("El id del Estado")
    private Long id;

    @ApiModelProperty("estado")
    private String estado;


    public StatusDto(Long id, String estado){

        this.id=id;
        this.estado=estado;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
