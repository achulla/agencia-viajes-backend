package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.FacturaDto;
import springboot.agencia.viajes.backend.app.repository.entity.Factura;

@Repository
public interface FacturaRepository extends JpaRepository<Factura, Long> {

    @Query("select new springboot.agencia.viajes.backend.app.dto.FacturaDto(" +
            "f.id," +
            "f.user.name," +
            "f.fechaRegistro" +
            ") from Factura f where f.flagActivo = true ")
    Page<FacturaDto> getAllByFactura(Pageable pageable);

    @Query("select new springboot.agencia.viajes.backend.app.dto.FacturaDto(" +
            "f.id," +
            "f.user.name," +
            "f.fechaRegistro" +
            ") from Factura f where concat(f.user.name) like %:filter% and f.flagActivo = true")
    Page<FacturaDto> getAllByFactura(@Param("filter") String filter, Pageable pageable);



}
