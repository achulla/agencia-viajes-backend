package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel("transport")
public class TransportDto {




    @ApiModelProperty("El id del transport")
    private Long id;

    @ApiModelProperty("La Placa del Vehiculo")
    private String placa;

    @ApiModelProperty("La Marca del Vehiculo")
    private String marca;

    @ApiModelProperty("Nombre del Chofer")
    private String chofer;

    @ApiModelProperty("tipo del vehiculo")
    private String tipo_vehiculo;

    @ApiModelProperty("Estado del Vehiculo")
    private String estado;

    public TransportDto(){}

    public TransportDto (Long id, String placa, String marca, String chofer, String tipo_vehiculo, String estado){

        this.id= id;
        this.placa= placa;
        this.marca= marca;
        this.chofer= chofer;
        this.tipo_vehiculo= tipo_vehiculo;
        this.estado= estado;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getChofer() {
        return chofer;
    }

    public void setChofer(String chofer) {
        this.chofer = chofer;
    }


    public String getTipo_vehiculo() {
        return tipo_vehiculo;
    }

    public void setTipo_vehiculo(String tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }




}
