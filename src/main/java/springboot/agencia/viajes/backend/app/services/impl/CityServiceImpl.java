package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.CityDto;
import springboot.agencia.viajes.backend.app.repository.entity.City;
import springboot.agencia.viajes.backend.app.repository.interfaces.CityRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.CityService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {


    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository){
        this.cityRepository=cityRepository;
    }

    @Override
    public List<CityDto> obtenerDtoList() {

        List<City> cityList  = cityRepository.findAll();
        List<CityDto> cityDtos = new ArrayList<>();

        cityList.stream().forEach(cityEntity -> {
            cityDtos.add(buildCityDto(cityEntity));
        });


        return cityDtos;
    }

    @Override
    public CityDto findByIdCity(Long id) {

        CityDto dto = new CityDto();
        Optional<City> city = cityRepository.findById(id);

        if (city.isPresent()){
            dto = buildCityDto(city.get());
        }

        return dto;
    }

    @Override
    public Long createCity(CityDto dto) {

        City entity = buildCityEntity(dto);
        entity = cityRepository.save(entity);

        return entity.getId();
    }

    @Override
    public CityDto updateCity(Long id, CityDto cityDto) {

        Optional<City> optionalCity = cityRepository.findById(id);

        CityDto dto = new CityDto();

        if (optionalCity.isPresent()){

            City city = optionalCity.get();

            city.setCiudad(cityDto.getCiudad());
            city.setPais(cityDto.getPais());

            city = cityRepository.save(city);

            dto = buildCityDto(city);

        }

        return dto;
    }

    @Override
    public void deleteById(Long id) {
        cityRepository.deleteById(id);
    }


    // metodo para construir el Dto atrraver de una entidad
    private CityDto buildCityDto ( City entity){

        CityDto dto = new CityDto();

        dto.setCiudad(entity.getCiudad());
        dto.setId(entity.getId());
        dto.setPais(entity.getPais());

        return dto;

    }

    // metodo para construir la entidad teniendo como imput un dto
    private City buildCityEntity ( CityDto dto) {

        City entity = new City();

        entity.setCiudad(dto.getCiudad());
        entity.setPais(dto.getPais());

        return entity;


    }


}
