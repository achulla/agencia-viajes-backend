package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ApiModel("schedule")
public class ScheduleDto {

    @ApiModelProperty("El id del schedule")
    private Long id;

    @ApiModelProperty("horario")
    private String hora;

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
//public ScheduleDto(){}

    public ScheduleDto(  Long id, String hora){
        this.id=id;
        this.hora=hora;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
