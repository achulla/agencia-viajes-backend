package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.ReservationDto;
import springboot.agencia.viajes.backend.app.repository.entity.Reservation;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

/*    @Query("select new springboot.agencia.viajes.backend.app.dto.ReservationDto ( " +
            "r.id," +
            "r.estado," +
            "r.fecha_registro," +
            "r.fecha_caducidad," +
            "r.user.id," +
            "r.user.name," +
            "r.user.lastname," +
            "r.transport.id," +
            "r.transport.chofer," +
            "r.schedule.id," +
            "r.schedule.fecha_viaje" +
             " ) from Reservation r ")
            List<ReservationDto> obtenerDtoList();*/



}
