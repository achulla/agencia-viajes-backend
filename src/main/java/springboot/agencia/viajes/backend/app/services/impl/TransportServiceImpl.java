package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.agencia.viajes.backend.app.dto.TransportDto;
import springboot.agencia.viajes.backend.app.repository.entity.Transport;
import springboot.agencia.viajes.backend.app.repository.interfaces.TransportRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.TransportService;
import springboot.agencia.viajes.backend.app.services.interfaces.UserService;

import java.util.List;
import java.util.Optional;


@Service
public class TransportServiceImpl implements TransportService {



    private final TransportRepository transportRepository;

    @Autowired
    public TransportServiceImpl(TransportRepository transportRepository) {

        this.transportRepository = transportRepository;
    }

    @Override
    public List<TransportDto> obtenerDtoList() {

        return transportRepository.obtenerDtoList();
    }


    @Override
    @Transactional("transactionManager")
    public TransportDto findByIdTransport(Long idTransport) {

        Optional <Transport> optionalTransport = transportRepository.findById(idTransport);

        return buildTransportDto(optionalTransport.get());


    }

    @Override
    @Transactional("transactionManager")
    public Long createTransport(TransportDto dto) {

        Transport transport = buildTransportEntity(dto);
        transport = transportRepository.save(transport);

        return transport.getId();
    }

    @Override
    @Transactional("transactionManager")
    public TransportDto updateTransport(Long id, TransportDto transportDto) {

        Optional<Transport> transportOptional = transportRepository.findById(id);

        Transport updateTransport = transportOptional.get();

        updateTransport.setChofer(transportDto.getChofer());
        updateTransport.setEstado(transportDto.getEstado());
        updateTransport.setMarca(transportDto.getMarca());
        updateTransport.setPlaca(transportDto.getPlaca());
        updateTransport.setPlaca(transportDto.getPlaca());
        updateTransport.setTipo_vehiculo(transportDto.getTipo_vehiculo());


        updateTransport = transportRepository.save(updateTransport);

        return buildTransportDto(updateTransport);
    }

    @Override
    @Transactional("transactionManager")
    public void deleteById(Long idTransport) {

        transportRepository.findById(idTransport);

    }

    /***********************METODOS PARA COMPLEMENTAR*****************************/
    // metodo para construir el Dto teniendo como entrada  una entidad
    private TransportDto buildTransportDto (Transport transport){

        TransportDto transportDto = new TransportDto();

        transportDto.setId(transport.getId());
        transportDto.setChofer(transport.getChofer());
        transportDto.setEstado(transport.getEstado());
        transportDto.setMarca(transport.getMarca());
        transportDto.setPlaca(transport.getPlaca());
        transportDto.setTipo_vehiculo(transport.getTipo_vehiculo());


        return transportDto;
    }


    // metodo para construir la entidad teniendo como dato de entrada un dto
    private Transport buildTransportEntity (TransportDto transportDto) {

        Transport transport = new Transport();
        transport.setChofer(transportDto.getChofer());
        transport.setEstado(transportDto.getEstado());
        transport.setMarca(transportDto.getMarca());
        transport.setPlaca(transportDto.getPlaca());
        transport.setTipo_vehiculo(transportDto.getTipo_vehiculo());

        return transport;

    }








}
