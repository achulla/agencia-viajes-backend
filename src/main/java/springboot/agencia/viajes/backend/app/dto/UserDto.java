package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;

@ApiModel("user")
public class UserDto {

    @ApiModelProperty("El id del Destino")
    private Long id;

    @ApiModelProperty("Nombre del usuario")
    private String name;

    @ApiModelProperty("Apellido del usuario")
    private String lastname;

    @ApiModelProperty("DNI del usuario")
    private String dni;

    @ApiModelProperty("Celular del usuario")
    private String cellphone;

    @ApiModelProperty("Email del usuario")
    private String email;

    @ApiModelProperty("usuario del usuario")
    private String username;

    @ApiModelProperty("contraseña del usuario")
    private String pass;



    public UserDto(){}

    public UserDto(Long id, String name, String lastname, String dni, String cellphone, String email, String username, String pass ){
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.dni = dni;
        this.cellphone = cellphone;
        this.email = email;
        this.username=username;
        this.pass=pass;


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


}
