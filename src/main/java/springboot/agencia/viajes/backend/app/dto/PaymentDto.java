package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("payment")
public class PaymentDto {
    @ApiModelProperty("El id del schedule")
    private Long id;

    @ApiModelProperty("El codigo del voucher a validar")
    private String codigo_voucher;

    @ApiModelProperty("una descripcion")
    private String descripcion;

    @ApiModelProperty("el numero de la reserva")
    private Long reservation_id;

    @ApiModelProperty("la fecha en que reservo")
    private Date fecha_registro;

    public PaymentDto(Long id, String codigo_voucher, String descripcion, Long reservation_id, Date fecha_registro){
        this.id=id;
        this.codigo_voucher=codigo_voucher;
        this.descripcion=descripcion;
        this.reservation_id=reservation_id;
        this.fecha_registro=fecha_registro;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo_voucher() {
        return codigo_voucher;
    }

    public void setCodigo_voucher(String codigo_voucher) {
        this.codigo_voucher = codigo_voucher;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(Long reservation_id) {
        this.reservation_id = reservation_id;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
}
