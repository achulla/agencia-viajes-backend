package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.repository.entity.City;


@Repository
public interface CityRepository extends JpaRepository<City, Long> {


}
