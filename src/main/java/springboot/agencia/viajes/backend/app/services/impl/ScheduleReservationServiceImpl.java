package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.ResponseScheduleDto;
import springboot.agencia.viajes.backend.app.dto.ScheduleReservationDto;
import springboot.agencia.viajes.backend.app.repository.entity.Price;
import springboot.agencia.viajes.backend.app.repository.entity.Schedule;
import springboot.agencia.viajes.backend.app.repository.interfaces.PriceRepository;
import springboot.agencia.viajes.backend.app.repository.interfaces.ScheduleRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.ScheduleReservationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ScheduleReservationServiceImpl implements ScheduleReservationService {

    private final PriceRepository priceRepository;
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public ScheduleReservationServiceImpl(PriceRepository priceRepository, ScheduleRepository scheduleRepository){
        this.priceRepository = priceRepository;
        this.scheduleRepository=scheduleRepository;
    }

    @Override
    public ResponseScheduleDto getSchedules(String origen, String destination) {

        ResponseScheduleDto responseScheduleDto = new ResponseScheduleDto();
        //responseScheduleDto.setTravelerDate(travelerDate);

        //llamada a todos los horarios
        List<Schedule> schedules = scheduleRepository.findAll();
        // llamada al repositorio de precios
        List<Price> prices = priceRepository.findAll();

        // filtrando por el origen y destino
        Optional<Price> filter = prices.stream()
                .filter(price -> (destination.equalsIgnoreCase(String.valueOf(price.getId_destino().getId())))
                        & origen.equalsIgnoreCase(String.valueOf(price.getId_origen().getId())))
                .findFirst();

        Price entityPrice;
        List<ScheduleReservationDto> scheduleReservations = new ArrayList<>();

        if (filter.isPresent()) {
            entityPrice = filter.get();


            String destinationFinal = entityPrice.getId_destino().getCity().getCiudad();
            String origin = entityPrice.getId_origen().getCity().getCiudad();
            String price = entityPrice.getImporte();
            String transportType = entityPrice.getId_transport().getTipo_vehiculo();
            String identifierPrice = String.valueOf(entityPrice.getId());

            // llenar el horario
            schedules.forEach(schedule -> {

                ScheduleReservationDto scheduleReservation = new ScheduleReservationDto();

                scheduleReservation.setOrigen(origin);
                scheduleReservation.setDestination(destinationFinal);
                scheduleReservation.setHour(schedule.getHora());
                scheduleReservation.setPrice(price);
                scheduleReservation.setTransportType(transportType);
                scheduleReservation.setIdentifierPrice(identifierPrice);

                scheduleReservations.add(scheduleReservation);


            });

        }
        responseScheduleDto.setResponse(scheduleReservations);
        return responseScheduleDto;
    }
}
