package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.PaymentDto;
import springboot.agencia.viajes.backend.app.repository.interfaces.PaymentRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.PaymentService;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }


    @Override
    public List<PaymentDto> obtenerDtoList() {
        return paymentRepository.obtenerDtoList();
    }

    @Override
    public PaymentDto findById(Long id) {
        return null;
    }

    @Override
    public Long createReservation(PaymentDto dto) {
        return null;
    }

    @Override
    public PaymentDto updatePayment(Long id, PaymentDto paymentDto) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }
}
