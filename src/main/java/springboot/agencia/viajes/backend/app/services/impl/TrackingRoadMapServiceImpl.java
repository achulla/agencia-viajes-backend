package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.agencia.viajes.backend.app.dto.TrackingRoadMapDto;
import springboot.agencia.viajes.backend.app.repository.interfaces.TrackingRoadMapRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.TrackingRoadMapService;

import java.util.List;

@Service
public class TrackingRoadMapServiceImpl implements TrackingRoadMapService {

    private final TrackingRoadMapRepository trackingRoadMapRepository;

    @Autowired
    public TrackingRoadMapServiceImpl(TrackingRoadMapRepository trackingRoadMapRepository) {
        this.trackingRoadMapRepository = trackingRoadMapRepository;
    }


    @Override
    public List<TrackingRoadMapDto> getObtenerListDto() {
        return trackingRoadMapRepository.findAllBy();

    }



}
