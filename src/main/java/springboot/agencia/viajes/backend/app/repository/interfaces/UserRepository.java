package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.UserDto;
import springboot.agencia.viajes.backend.app.repository.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    @Query("select new springboot.agencia.viajes.backend.app.dto.UserDto (" +
            "u.id," +
            "u.name," +
            "u.lastname," +
            "u.dni," +
            "u.cellphone," +
            "u.email," +
            "u.username," +
            "u.pass) from User u  ")
    List<UserDto> obtenerDtoList();



/*    @Query("select new springboot.agencia.viajes.backend.app.dto.UserDto (" +
           "u.id," +
           "u.name," +
           "u.lastname," +
           "u.dni," +
           "u.cellphone," +
           "u.email ) from User u  ")
            List<UserDto> obtenerDtoList();*/

/*    @Query("select new springboot.agencia.viajes.backend.app.dto.UserDto (" +
            "u.id," +
            "u.name," +
            "u.lastname," +
            "u.dni," +
            "u.cellphone," +
            "u.email ) from User u  where u.id =:id order by u.id desc")
    Page<UserDto> findDtoById(@Param("id") Long id);*/

/*    @Query("delete  from User u  where u.id = ?1 ")
        void deleteById(Long id);*/

/*    @Query("SELECT * FROM User C WHERE C.email = :email AND C.password = :password ")
    User findByUserAndPassword (@Param("email") String email, @Param("password") String password);*/

}
