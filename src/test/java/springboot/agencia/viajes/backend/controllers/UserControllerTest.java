package springboot.agencia.viajes.backend.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import springboot.agencia.viajes.backend.AgenciaViajesBackendApplication;
import springboot.agencia.viajes.backend.config.constans.ApiConstant;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AgenciaViajesBackendApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @Autowired
    WebApplicationContext context;

    @Autowired(required = false)
    FilterChainProxy springSecurityFilterChain;

    @Autowired
    ObjectMapper objectMapper;

    private MockMvc mockMvc;

   // private Faker faker;
    private int origen = 1;
    private int destino = 3;
   // private String fecha = "08-08-2020";


    @Test
    public void GetUser() throws Exception {
        mockMvc.perform(get(ApiConstant.BASE_PATH  + '/' + origen  + '/'  + destino)
                .contentType(ApiConstant.CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());
    }






}
