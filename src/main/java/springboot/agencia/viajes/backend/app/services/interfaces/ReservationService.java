package springboot.agencia.viajes.backend.app.services.interfaces;


import springboot.agencia.viajes.backend.app.dto.PaymentReservationDto;
import springboot.agencia.viajes.backend.app.dto.ReservationDto;

import java.util.List;

public interface ReservationService {


    List<ReservationDto> obtenerDtoList();

     ReservationDto findById(Long id);

    Long createReservation(ReservationDto dto);

    ReservationDto updateReservation(Long id, ReservationDto reservationDto);


     void deleteById(Long id);

    ReservationDto saveReservation(ReservationDto reservationDto);

    ReservationDto payment (PaymentReservationDto dto);



}
