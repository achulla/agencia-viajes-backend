package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.UserDto;
import springboot.agencia.viajes.backend.app.services.interfaces.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/user")
@Api(tags = "Rest Api User", consumes = "application/json")
public class UserController {


    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar los usuarios")
    public List<UserDto> obtenerDtoList(){
        return userService.obtenerDtoList();
    }

    @GetMapping("/{idUser}")
    @ApiOperation(value = "Detalle", notes = "Detalle de un usuario por Id")
    public UserDto detailUser(@PathVariable("idUser") Long idUser){

        return userService.findByIdUser(idUser);
    }


    @PostMapping
    @ApiOperation(value = "Crear", notes = "Registrar un nuevo Usuario")
    public UserDto createUser(@RequestBody @Valid UserDto userDto){
        Long userId = userService.createUser(userDto);

        return userService.findByIdUser(userId);
    }


    @PutMapping("/{idUser}")
    @ApiOperation(value = "Modificar", notes = "Modifica un usuario por Id")
    public UserDto updateUser(@PathVariable("idUser") Long idUser, @RequestBody @Validated UserDto userDto){


        return userService.updateUser(idUser, userDto);
    }


    @DeleteMapping("/{idUser}")
    @ApiOperation(value = "Eliminar", notes = "Elimina un usuario por Id")
    public void deleteUser(@PathVariable("idUser") Long idUser){

        userService.deleteById(idUser);
    }



















/*
    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar los usuarios")
    public List<UserDto> obtenerDtoList(){
        return userService.obtenerDtoList();
    }

    @GetMapping("/{idUser}")
    @ApiOperation(value = "Detalle", notes = "Detalle de un usuario por Id")
    public UserDto detailUser(@PathVariable("idUser") Long idUser){

        return userService.findByIdUser(idUser);
    }


    @PostMapping
    @ApiOperation(value = "Crear", notes = "Registrar un nuevo Usuario")
    public UserDto createUser(@RequestBody @Valid UserDto userDto){
        Long userId = userService.createUser(userDto);

        return userService.findByIdUser(userId);
    }


    @PutMapping("/{idUser}")
    @ApiOperation(value = "Modificar", notes = "Modifica un usuario por Id")
    public UserDto updateUser(@PathVariable("idUser") Long idUser, @RequestBody @Validated UserDto userDto){


        return userService.updateUser(idUser, userDto);
    }


    @DeleteMapping("/{idUser}")
    @ApiOperation(value = "Eliminar", notes = "Elimina un usuario por Id")
    public void deleteUser(@PathVariable("idUser") Long idUser){

        userService.deleteById(idUser);
    }*/

}
