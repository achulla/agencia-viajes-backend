package springboot.agencia.viajes.backend.app.services.interfaces;

import springboot.agencia.viajes.backend.app.dto.ResponseScheduleDto;

public interface ScheduleReservationService {

    ResponseScheduleDto getSchedules (String origen, String destination);
}
