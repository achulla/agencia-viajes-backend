package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import springboot.agencia.viajes.backend.app.dto.FacturaDetailsDto;
import springboot.agencia.viajes.backend.app.dto.FacturaDto;
import springboot.agencia.viajes.backend.app.dto.FacturaRequestDto;
import springboot.agencia.viajes.backend.app.mapper.ConfigurationMapper;
import springboot.agencia.viajes.backend.app.repository.entity.Factura;
import springboot.agencia.viajes.backend.app.repository.entity.User;
import springboot.agencia.viajes.backend.app.repository.interfaces.FacturaRepository;
import springboot.agencia.viajes.backend.app.repository.interfaces.UserRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.FacturaService;
import springboot.agencia.viajes.backend.config.exceptions.AssociatedResourceNotFoundException;
import springboot.agencia.viajes.backend.config.exceptions.ResourceNotFoundException;
import springboot.agencia.viajes.backend.util.MessageUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class FacturaServiceImpl implements FacturaService {

    private final FacturaRepository facturaRepository;
    private final UserRepository userRepository;
    private final MessageUtil messageUtil;
    private final ConfigurationMapper configurationMapper;

    @Autowired
    public FacturaServiceImpl(FacturaRepository facturaRepository,
                              UserRepository userRepository,
                              MessageUtil messageUtil,
                              ConfigurationMapper configurationMapper) {
        this.facturaRepository = facturaRepository;
        this.userRepository = userRepository;
        this.messageUtil = messageUtil;
        this.configurationMapper = configurationMapper;
    }

    @Override
    public Page<FacturaDto> getFacturas(String filter, Pageable pageable) {
        if (!StringUtils.isEmpty(filter)){
            return facturaRepository.getAllByFactura(filter, pageable);
        }
        return facturaRepository.getAllByFactura(pageable);
    }

    @Override
    public FacturaDetailsDto buscarFactura(Long idFactura) {
        Factura factura = facturaRepository.findById(idFactura).orElseThrow(() ->
                new ResourceNotFoundException(messageUtil.getMessageByCode("No se Encontro la Factura")));
        return configurationMapper.facturaDetailsDto(factura);
    }

    @Override
    @Transactional
    public FacturaDetailsDto saveFactura(FacturaRequestDto facturaRequestDto) {
        Factura factura = new Factura();
        setCreateOrUpdate(factura, facturaRequestDto);
        facturaRepository.save(factura);
        return configurationMapper.facturaDetailsDto(factura);
    }

    private void setCreateOrUpdate(Factura factura, FacturaRequestDto facturaRequestDto){
        User user = userRepository.findById(facturaRequestDto.getId_user()).orElseThrow(() ->
                new AssociatedResourceNotFoundException(messageUtil.getMessageByCode("No se encontro el Usuario")));
        factura.setFechaRegistro(new Date());
        factura.setUser(user);
        System.out.println(user.getId());
    }

    @Override
    public FacturaDetailsDto updateFactura(FacturaRequestDto facturaRequestDto, Long idFactura) {
        Factura facturaUpdate = facturaRepository.findById(idFactura)
                .orElseThrow(() -> new ResourceNotFoundException(messageUtil.getMessageByCode("no se encontro la factura")));
        System.out.println(facturaUpdate.getId());
        setCreateOrUpdate(facturaUpdate, facturaRequestDto);
        facturaUpdate.setFechaModificacion(new Date());
        facturaRepository.save(facturaUpdate);
        return configurationMapper.facturaDetailsDto(facturaUpdate);
    }

    @Override
    @Transactional
    public void deleteFactura(Long idFactura) {
        Factura dto = facturaRepository.findById(idFactura)
                .orElseThrow(() -> new ResourceNotFoundException(messageUtil.getMessageByCode("no se encontro la factura")));
        dto.setFlagActivo(false);
    }

}
