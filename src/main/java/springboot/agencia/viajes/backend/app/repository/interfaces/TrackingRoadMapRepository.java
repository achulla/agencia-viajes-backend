package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.TrackingRoadMapDto;
import springboot.agencia.viajes.backend.app.repository.entity.TrackingRoadMap;

import java.util.List;

@Repository
public interface TrackingRoadMapRepository extends JpaRepository<TrackingRoadMap, Long> {

/*
    @Query("select new springboot.agencia.viajes.backend.app.dto.TrackingRoadMapDto (" +
            "t.id," +
            "t.latitude," +
            "t.longitude" +
            " ) from TrackingRoadMap t")
    List<TrackingRoadMapDto> obtenerListDto();
*/

        List<TrackingRoadMapDto> findAllBy();

}
