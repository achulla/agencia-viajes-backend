package springboot.agencia.viajes.backend.config.sort;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class SortMapUtil {

    private final Map<String, String> facturaSort = new HashMap<>();

    @PostConstruct
    public void init(){
        facturaSort.put("id", "id");
        facturaSort.put("user", "user.name");
        facturaSort.put("fechaRegistro", "fechaRegistro");
    }

    public Pageable updatePagination(Pageable old, SortEntityEnum type) {
        if (old.getSort().isUnsorted())
            return old;
        String propertySort = null;
        Sort.Direction directionSort = null;
        for (Sort.Order order : old.getSort()) {
            propertySort = order.getProperty();
            directionSort = order.getDirection();
        }

        switch (type) {
            case FACTURA:
                return PageRequest.of(old.getPageNumber(), old.getPageSize(), directionSort, facturaSort.get(propertySort));
            default:
                return old;
        }
    }

}
