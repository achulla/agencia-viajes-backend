package springboot.agencia.viajes.backend.app.services.interfaces;



import springboot.agencia.viajes.backend.app.dto.StatusDto;

import java.util.List;

public interface StatusService {

    /**
     * Obtiene Todos los datos del usuario
     *
     */
    List<StatusDto> obtenerDtoList();


}
