package springboot.agencia.viajes.backend.app.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.agencia.viajes.backend.app.dto.TrackingRoadMapDto;
import springboot.agencia.viajes.backend.app.services.interfaces.TrackingRoadMapService;

import java.util.List;

@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/tracking")
@Api(tags = "Rest Api Tracking", consumes = "application/json")
public class TrackingRoadMapController {

    private final TrackingRoadMapService trackingRoadMapService;

    @Autowired
    public TrackingRoadMapController(TrackingRoadMapService trackingRoadMapService) {
        this.trackingRoadMapService = trackingRoadMapService;
    }

    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar los usuarios")
    public List<TrackingRoadMapDto> getTracking(){
        return trackingRoadMapService.getObtenerListDto();
    }

}
