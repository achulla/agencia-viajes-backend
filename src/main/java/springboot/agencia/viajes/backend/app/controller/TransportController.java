package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.TransportDto;
import springboot.agencia.viajes.backend.app.services.interfaces.TransportService;

import javax.validation.Valid;
import java.util.List;



@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/transport")
@Api(tags = "Rest Api Transport", consumes = "application/json")
public class TransportController {


    private final TransportService transportService;

    @Autowired
    public TransportController(TransportService transportService) {
        this.transportService = transportService;
    }

    @GetMapping("/listar")
    @ApiOperation(value = "listado", notes = "listar los usuarios")
    public List<TransportDto> obtenerDtoList() {
        return transportService.obtenerDtoList();
    }

    @GetMapping("/{idTransport}")
    @ApiOperation(value = "Detalle", notes = "Detalle de un transporte por Id")
    public TransportDto detailTransport (@PathVariable("idTransport")Long idTransport){
        return transportService.findByIdTransport(idTransport);
    }

    @PostMapping
    @ApiOperation(value = "crear", notes = "Registrar un nuevo transporte")
    public TransportDto createTransport(@RequestBody @Valid TransportDto transportDto){
        Long transportId =transportService.createTransport(transportDto);
        return transportService.findByIdTransport(transportId);
    }

    @PutMapping("/{idTransport}")
    @ApiOperation(value = "Modificar", notes = "Modificar un transporte by Id")
    public TransportDto updateTransport(@PathVariable("idTransport") Long idTransport, @RequestBody @Validated TransportDto transportDto){
        return  transportService.updateTransport(idTransport, transportDto);
    }

    @DeleteMapping("/{idTransport}")
    @ApiOperation(value = "Eliminar", notes = "Eliminar un transporte by Id")
    public void deleteTransport (@PathVariable("idTransport") Long idTransport){
        transportService.deleteById(idTransport);
    }




}
