package springboot.agencia.viajes.backend.app.repository.entity;

public enum StatusType {

    TO_PAY(1L, "TO_PAY","Por pagar"),
    PAID(2L, "PAID","Pagado");

    private final Long code;
    private final String value;
    private final String description;

    StatusType(Long code, String value ,String description) {
        this.code = code;
        this.value = value;
        this.description = description;
    }

    public static StatusType fromValue(String value) {
        for (StatusType statusType : values()) {
            if (statusType.value.equalsIgnoreCase(value)) {
                return statusType;
            }
        }
        return null;
    }

}
