package springboot.agencia.viajes.backend.app.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.agencia.viajes.backend.app.dto.PriceDto;
import springboot.agencia.viajes.backend.app.services.interfaces.PriceService;

import java.util.List;


@RestController
@RequestMapping("/api/price")
@CrossOrigin(origins= "http://localhost:4200")
@Api(tags = "Rest Api Price", consumes = "application/json")
public class PriceController {

    private final PriceService priceService;

    @Autowired
    public PriceController(PriceService priceService){

        this.priceService = priceService;
    }
    @GetMapping("/listar")
    @ApiOperation(value = "listado", notes = "listar los precios")
    public List<PriceDto> obtenerDtoList(){

        return priceService.obtenerDtoList();
    }


}
