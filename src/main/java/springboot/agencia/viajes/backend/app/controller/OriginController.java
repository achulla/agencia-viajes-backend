package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.OriginDto;
import springboot.agencia.viajes.backend.app.services.interfaces.OriginService;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/origin")
@CrossOrigin(origins= "http://localhost:4200")
@Api(tags = "Rest Api Origin", consumes = "application/json")
public class OriginController {


    private final OriginService originService;

    @Autowired
    public OriginController(OriginService originService){
        this.originService = originService;
    }

    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar el Origen")
    public List<OriginDto> obtenerDtoList(){
        return originService.obtenerDtoList();
    }

    /*
    @GetMapping("/{idOrigin}")
    @ApiOperation(value = "Detalle", notes = "Detalle de un origen por Id")
    public OriginDto detailOrigin(@PathVariable("idOrigin") Long idOrigin){
        return originService.findByIdOrigin(idOrigin);
    }
     */






}
