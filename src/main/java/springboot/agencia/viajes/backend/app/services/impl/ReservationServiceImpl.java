package springboot.agencia.viajes.backend.app.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.PaymentReservationDto;
import springboot.agencia.viajes.backend.app.dto.ReservationDto;
import springboot.agencia.viajes.backend.app.repository.entity.*;
import springboot.agencia.viajes.backend.app.repository.interfaces.PriceRepository;
import springboot.agencia.viajes.backend.app.repository.interfaces.ReservationRepository;
import springboot.agencia.viajes.backend.app.repository.interfaces.UserRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.ReservationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {


    private final ReservationRepository reservationRepository;
    private final PriceRepository priceRepository;
    private final UserRepository userRepository;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository, PriceRepository priceRepository, UserRepository userRepository) {
        this.reservationRepository = reservationRepository;
        this.priceRepository = priceRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<ReservationDto> obtenerDtoList() {

       List<Reservation> reservationList = reservationRepository.findAll();
        List<ReservationDto> reservationDtos = new ArrayList<>();
        reservationList.stream().forEach(reservationEntity->{
            reservationDtos.add(buildReservationDto(reservationEntity));});

        return reservationDtos;
    }



    @Override
    public ReservationDto findById(Long id) {
        return null;
    }

    @Override
    public Long createReservation(ReservationDto dto) {
        return null;
    }

    @Override
    public ReservationDto updateReservation(Long id, ReservationDto reservationDto) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public ReservationDto saveReservation(ReservationDto reservationDto) {

        // busqueda por id de precio
       Price price = priceRepository.getOne(Long.valueOf(reservationDto.getIdentifierPrice()));

       // busqueda por dni del usuario
        List<User> users = userRepository.findAll();

        //filtrar por dni
        Optional<User> optionalUser =
                users.stream()
                        .filter(user -> (reservationDto.getDni().equalsIgnoreCase(user.getDni())))
                        .findFirst();

        if (optionalUser.isPresent()){

            User user = optionalUser.get();
            Reservation reservation = buildReservationEntity(reservationDto, price, user);
            reservationRepository.save(reservation);
        }


        return reservationDto;
    }

    @Override
    public ReservationDto payment(PaymentReservationDto dto) {


        ReservationDto reservationDto = new ReservationDto();
        //busqueda de reservacion
        List<Reservation> reservations = reservationRepository.findAll();

        // filtro por la numero de reservacion
        Optional<Reservation> optionalReservation =
                reservations.stream()
                        .filter(reservation -> (dto.getIdReservation().equalsIgnoreCase(reservation.getId_reservation())))
                        .findFirst();

        if (optionalReservation.isPresent()){

            Reservation reservation = optionalReservation.get();

            Status status  = new Status();
            status.setEstado(dto.getStatus());
            status.setId(2l);

            reservation.setEstado(status);

            reservationDto = buildReservationDto(reservation);

            reservationRepository.save(reservation);
        }

        reservationDto.setEstado(dto.getStatus());


        return reservationDto;
    }

    // metodo para contruir el DTO atravez de una identidad

  private ReservationDto buildReservationDto (Reservation entity) {
        ReservationDto dto = new ReservationDto();
        dto.setEstado(entity.getEstado().getEstado());
        dto.setFecha_caducidad(entity.getFecha_caducidad());
        dto.setFecha_registro(entity.getFecha_registro());
        dto.setFecha_viaje(entity.getFecha_viaje());
        dto.setId(entity.getId());
        dto.setImporte(Double.parseDouble(entity.getPrice().getImporte()));
        dto.setLastname(entity.getUser().getLastname());
        dto.setName(entity.getUser().getName());
        dto.setTipo_transporte(entity.getPrice().getId_transport().getTipo_vehiculo());
        dto.setHora(entity.getSchedule().getHora());
        dto.setDni(entity.getUser().getDni());
        dto.setIdReservation(entity.getId_reservation());
        dto.setOrigen(entity.getPrice().getId_origen().getCity().getCiudad());
        dto.setDestino(entity.getPrice().getId_destino().getCity().getCiudad());
        return dto;
    }

    private Reservation buildReservationEntity (ReservationDto dto, Price price, User user) {
        Reservation entity = new Reservation();
        Status status = new Status();
        status.setId(1L);
        status.setEstado("por pagar");
        entity.setEstado(status);
        entity.setFecha_caducidad(dto.getFecha_caducidad());
        entity.setFecha_registro(dto.getFecha_registro());
        entity.setId_reservation(dto.getIdReservation());
        entity.setFecha_viaje(dto.getFecha_viaje());

        Schedule schedule = new Schedule();
        schedule.setId(1L);
        schedule.setHora("8:00 AM");

        entity.setSchedule(schedule);
        entity.setPrice(price);
        entity.setUser(user);


        return entity;
    }



}
