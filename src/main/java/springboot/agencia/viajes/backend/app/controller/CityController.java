package springboot.agencia.viajes.backend.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springboot.agencia.viajes.backend.app.dto.CityDto;
import springboot.agencia.viajes.backend.app.services.interfaces.CityService;

import javax.validation.Valid;
import java.util.List;




@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/city")
@Api(tags = "Rest Api City", consumes = "application/json")
public class CityController {


    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService){
        this.cityService = cityService;
    }

    @GetMapping("/listar")
    @ApiOperation(value = "listado", notes = "listar las ciudades")
    public List<CityDto> obtenerDtoList(){
        return cityService.obtenerDtoList();
    }

    @GetMapping("/{idCity}")
    @ApiOperation(value = "Detalle", notes = "Detalle de una ciudad BY id")
    public CityDto detailCity(@PathVariable("idCity") Long idCity){
        return  cityService.findByIdCity(idCity);
    }

    @PostMapping
    @ApiOperation(value = "crear", notes = "Registrar una nueva ciudad")
    public CityDto createCity(@RequestBody @Valid CityDto cityDto){
        Long cityId = cityService.createCity(cityDto);

        return cityService.findByIdCity(cityId);
    }

    @PutMapping("/{idCity}")
    @ApiOperation(value = "Modificar", notes = "Modifica una ciudad By id")
    public CityDto updateCity(@PathVariable("idCity") Long idCity, @RequestBody @Validated CityDto cityDto){
        return cityService.updateCity(idCity, cityDto);
    }

    @DeleteMapping("/{idCity")
    @ApiOperation(value = "Eliminar", notes = "Elimina una ciudad BY id")
    public void deleteCity(@PathVariable("idCity") Long idCity){
        cityService.deleteById(idCity);
    }







}
