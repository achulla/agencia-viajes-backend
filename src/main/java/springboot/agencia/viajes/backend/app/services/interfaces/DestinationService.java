package springboot.agencia.viajes.backend.app.services.interfaces;


import springboot.agencia.viajes.backend.app.dto.DestinationDto;

import java.util.List;

public interface DestinationService {


/**
 * Obtiene Todos los datos del Destino
 *
 */

    List<DestinationDto> obtenerDtoList();


/**
 * Obtiene los datos de la moneda
 *
 * @param idDest Id del Destino
 *      * @return Destino
 */

    DestinationDto findByIdDest(Long idDest);


/**
 * Registra los  Destino
 *
 * @param dto Destino
 * @return registrar Destinos
 */

/*    Long createDest(DestinationDto dto);*/


/**
 * Modifica un Destinos segun los datos enviados.
 *
 * @param idDest  Id del Destinos
 * @param destinationDto Destinos
 * @return Los campos del Destinos seran modificados
 */

/*    DestinationDto updateDest(Long idDest, DestinationDto destinationDto);*/

/**
 * Eliminar un Destinos del repositorio de datos
 *
 * @param idDest Id del Destinos
 */

/*    void deleteById(Long idDest);*/



}
