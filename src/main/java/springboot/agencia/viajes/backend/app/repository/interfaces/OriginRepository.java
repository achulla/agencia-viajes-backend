package springboot.agencia.viajes.backend.app.repository.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.DestinationDto;
import springboot.agencia.viajes.backend.app.dto.OriginDto;
import springboot.agencia.viajes.backend.app.repository.entity.Origin;

import java.util.List;

@Repository
public interface OriginRepository extends JpaRepository<Origin, Long> {

    @Query("select new springboot.agencia.viajes.backend.app.dto.OriginDto (" +
            "o.id," +
            "o.city.ciudad ) from Origin o ")
    List<OriginDto> obtenerDtoList();


/*    @Query("select new springboot.agencia.viajes.backend.app.dto.OriginDto (" +
            "o.id," +
            "o.city,) from Origin o  where o.id =:id order by o.id desc")
    Page<OriginDto> findDtoById(@Param("id") Long id);*/


}
