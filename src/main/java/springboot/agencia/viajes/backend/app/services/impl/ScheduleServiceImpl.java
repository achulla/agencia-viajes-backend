package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.ScheduleDto;
import springboot.agencia.viajes.backend.app.repository.interfaces.ScheduleRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.ScheduleService;

import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduleRepository scheduleRepository;


    @Autowired
    public ScheduleServiceImpl(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }


    @Override
    public List<ScheduleDto> obtenerDtoList() {

        return scheduleRepository.obtenerDtoList();
    }



    @Override
    public ScheduleDto findById(Long id) {
        return null;
    }

    @Override
    public Long createSchedule(ScheduleDto dto) {
        return null;
    }

    @Override
    public ScheduleDto updateSchedule(Long id, ScheduleDto scheduleDto) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }





}
