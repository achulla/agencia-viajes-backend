package springboot.agencia.viajes.backend.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.agencia.viajes.backend.app.dto.UserDto;
import springboot.agencia.viajes.backend.app.repository.entity.User;
import springboot.agencia.viajes.backend.app.repository.interfaces.UserRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public List<UserDto> obtenerDtoList() {

        return userRepository.obtenerDtoList();
    }


    @Override
    @Transactional("transactionManager")
    public UserDto findByIdUser(Long idUser) {
        Optional<User> userOptional = userRepository.findById(idUser);

        // User user = userOptional.get();
        return buildUserDto(userOptional.get());
    }



    @Override
    @Transactional("transactionManager")
    public Long createUser(UserDto dto) {

        User user = buildUserEntity(dto);
        user = userRepository.save(user);

        return user.getId();
    }

    @Override
    @Transactional("transactionManager")
    public UserDto updateUser(Long idUser, UserDto userDto) {

        Optional<User> userOptional = userRepository.findById(idUser);
        User updateUser = userOptional.get();

        updateUser.setName(userDto.getName());
        updateUser.setLastname(userDto.getLastname());
        updateUser.setDni(userDto.getDni());
        updateUser.setCellphone(userDto.getCellphone());
        updateUser.setEmail(userDto.getEmail());
        updateUser.setUsername(userDto.getUsername());
        updateUser.setPass(userDto.getPass());


        updateUser = userRepository.save(updateUser);

        return buildUserDto(updateUser);
    }


    @Override
    @Transactional("transactionManager")
    public void deleteById(Long idUser) {
        Optional<User> deleteUser = userRepository.findById(idUser);
        User user = deleteUser.get();

        userRepository.save(user);

        userRepository.deleteById(idUser);
    }



    /***********************METODOS PARA COMPLEMENTAR*****************************/


    private UserDto buildUserDto(User user){

        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setLastname(user.getLastname());
        userDto.setDni(user.getDni());
        userDto.setCellphone(user.getCellphone());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPass(user.getPass());

        return userDto;
    }
    private User buildUserEntity (UserDto dto) {

        User user = new User();
        user.setName(dto.getName());
        user.setLastname(dto.getLastname());
        user.setDni(dto.getDni());
        user.setCellphone(dto.getCellphone());
        user.setEmail(dto.getEmail());
        user.setUsername(dto.getUsername());
        user.setPass(dto.getPass());

        return user;

    }





}
