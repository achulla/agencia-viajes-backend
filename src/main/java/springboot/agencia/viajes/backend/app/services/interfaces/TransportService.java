package springboot.agencia.viajes.backend.app.services.interfaces;

import springboot.agencia.viajes.backend.app.dto.TransportDto;

import java.util.List;

public interface TransportService {


    /**
     * Obtiene Todos los datos de transporte
     *
     */
    List<TransportDto> obtenerDtoList();


    /**
     * Obtiene los datos del transporte
     *
     * @param id Id del transporte
     *      * @return transporte
     */
    TransportDto findByIdTransport(Long id);



    /**
     * Registrar transporte
     *
     * @param dto User
     * @return registro de un transporte
     */
    Long createTransport(TransportDto dto);



    /**
     * Modifica un transporte segun los datos enviados.
     *
     * @param id  Id del transporte
     * @param transportDto user
     * @return Los campos del transporte seran modificados
     */
    TransportDto updateTransport(Long id, TransportDto transportDto);


    /**
     * Eliminar un transporte del repositorio de datos
     *
     * @param id Id del transporte
     */

    void deleteById(Long id);





}
