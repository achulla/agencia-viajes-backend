package springboot.agencia.viajes.backend.app.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.agencia.viajes.backend.app.dto.ScheduleDto;
import springboot.agencia.viajes.backend.app.services.interfaces.ScheduleService;

import java.util.List;

@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping("/api/schedule")
@Api(tags = "Rest Api Schedule", consumes = "application/json")
public class ScheduleController {

    private final ScheduleService scheduleService;

    @Autowired
    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }



    @GetMapping("/listar")
    @ApiOperation(value = "Listado", notes = "Listar los Horarios")
    public List<ScheduleDto> obtenerDtoList(){
        return scheduleService.obtenerDtoList();
    }


}
