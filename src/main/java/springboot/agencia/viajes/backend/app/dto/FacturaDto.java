package springboot.agencia.viajes.backend.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("Resumen")
public class FacturaDto {

    @ApiModelProperty("El id de la factura")
    private Long id;

    @ApiModelProperty("El id del Usuario")
    private String user;

    @ApiModelProperty("Fecha de Registro")
    private Date fechaRegistro;

    public FacturaDto() {
    }

    public FacturaDto(Long id, String user, Date fechaRegistro) {
        this.id = id;
        this.user = user;
        this.fechaRegistro = fechaRegistro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
