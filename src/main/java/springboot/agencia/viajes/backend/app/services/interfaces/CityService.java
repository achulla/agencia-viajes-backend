package springboot.agencia.viajes.backend.app.services.interfaces;


import springboot.agencia.viajes.backend.app.dto.CityDto;


import java.util.List;


public interface CityService {


    /**
     * Obtiene Todos los datos de la ciudad
     *
     */
    List<CityDto> obtenerDtoList();


    /**
     * Obtiene los datos de la ciudad
     *
     * @param id Id del ciudad
     *      * @return ciudad
     */
    CityDto findByIdCity(Long id);



    /**
     * Registrar ciudad
     *
     * @param dto User
     * @return registro de una ciudad
     */
    Long createCity(CityDto dto);



    /**
     * Modifica una ciudad segun los datos enviados.
     *
     * @param id  Id de la ciudad
     * @param cityDto city
     * @return Los campos de la ciudad seran modificados
     */
    CityDto updateCity(Long id, CityDto cityDto);


    /**
     * Eliminar ciudad del repositorio de datos
     *
     * @param id Id de la ciudad
     */

    void deleteById(Long id);



}
