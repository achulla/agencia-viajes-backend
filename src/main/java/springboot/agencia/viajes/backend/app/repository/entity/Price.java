package springboot.agencia.viajes.backend.app.repository.entity;


import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "price")
public class Price {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_origen")
    private Origin id_origen;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_destino")
    private Destination id_destino;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_transport")
    private Transport id_transport;

    @Column(name = "importe")
    private String importe;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Origin getId_origen() {
        return id_origen;
    }

    public void setId_origen(Origin id_origen) {
        this.id_origen = id_origen;
    }

    public Destination getId_destino() {
        return id_destino;
    }

    public void setId_destino(Destination id_destino) {
        this.id_destino = id_destino;
    }

    public Transport getId_transport() {
        return id_transport;
    }

    public void setId_transport(Transport id_transport) {
        this.id_transport = id_transport;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

}
