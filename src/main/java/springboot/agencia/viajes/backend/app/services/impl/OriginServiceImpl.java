package springboot.agencia.viajes.backend.app.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.agencia.viajes.backend.app.dto.OriginDto;
import springboot.agencia.viajes.backend.app.repository.entity.Origin;
import springboot.agencia.viajes.backend.app.repository.interfaces.OriginRepository;
import springboot.agencia.viajes.backend.app.services.interfaces.OriginService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class OriginServiceImpl  implements OriginService {


    private final OriginRepository originRepository;

    @Autowired
    public OriginServiceImpl(OriginRepository originRepository) {
        this.originRepository = originRepository;
    }

    @Override
    public List<OriginDto> obtenerDtoList() {
        return originRepository.obtenerDtoList();
    }

    @Override
    @Transactional("transactionManager")
    public OriginDto findByIdOrigin(Long idOrigin) {
        Optional<Origin> originOptional = originRepository.findById(idOrigin);
        return originToDto(originOptional.get());
    }

    private OriginDto originToDto(Origin origin){
        OriginDto originDto = new OriginDto();
        originDto.setId(origin.getId());
        return originDto;
    }





}