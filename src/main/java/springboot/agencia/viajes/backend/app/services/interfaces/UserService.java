package springboot.agencia.viajes.backend.app.services.interfaces;


import springboot.agencia.viajes.backend.app.dto.UserDto;

import java.util.List;

public interface UserService {


    /**
     * Obtiene Todos los datos del usuario
     *
     */

    List<UserDto> obtenerDtoList();



    /**
     * Obtiene los datos de la moneda
     *
     * @param idUser Id del usuario
     *      * @return usuario
     */

    UserDto findByIdUser(Long idUser);



    /**
     * Registra los  usuario
     *
     * @param dto User
     * @return registro de un usuario
     */

    Long createUser(UserDto dto);



    /**
     * Modifica un usuario segun los datos enviados.
     *
     * @param idUser  Id del user
     * @param userDto user
     * @return Los campos del user seran modificados
     */

    UserDto updateUser(Long idUser, UserDto userDto);


    /**
     * Eliminar un usuario del repositorio de datos
     *
     * @param idUser Id del user
     */

    void deleteById(Long idUser);








}
