package springboot.agencia.viajes.backend.app.repository.interfaces;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import springboot.agencia.viajes.backend.app.dto.PaymentDto;
import springboot.agencia.viajes.backend.app.repository.entity.Payment;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {


    @Query("select new springboot.agencia.viajes.backend.app.dto.PaymentDto (" +
            "p.id," +
             "p.codigo_voucher," +
             "p.descripcion," +
             "p.reservation.id," +
             "p.reservation.fecha_registro " +
            ") from Payment p ")
            List<PaymentDto> obtenerDtoList();


}
