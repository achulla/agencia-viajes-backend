package springboot.agencia.viajes.backend.config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class AssociatedResourceNotFoundException extends RuntimeException{
    public AssociatedResourceNotFoundException(String msg) {
        super(msg);
    }
}
